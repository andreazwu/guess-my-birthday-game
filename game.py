from random import randint

print ("Guess My Birthday Game")

name = input ("Hi! What is your name?")

for guess_number in range(1,6):
    guess_month = randint(1,12)
    guess_year = randint(1980,2022)

    print ("Guess", guess_number, ":", name, "were you born in", guess_month, "/", guess_year, "?")

    answer = input ("yes or no?")

    if (answer == "yes"):
        print ("I knew it!")
        exit ()
    elif guess_number == 5:
        print ("I have other things to do. Goodbye.")
    else:
        print ("Drat! Lemme try again!")

